# Copyright 2021 Variscite LTD
# SPDX-License-Identifier: BSD-3-Clause
import argparse

import cv2
import numpy as np
from PIL import Image
import time
from tflite_runtime.interpreter import Interpreter

from helper.config import TITLE
from helper.opencv import put_info_on_frame, put_fps_on_frame
from helper.utils import load_labels, Timer

def image_classification(args):
    labels = load_labels(args['label'])

    interpreter = Interpreter(model_path=args['model'])
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    input_shape = [2,224,224,3]
    with Image.open(args['image']) as im:
        _, height, width, _ = input_details[0]['shape']
        image = np.array(im,dtype=np.float32)
        image = image[:, :, ::-1].copy()
        interpreter.resize_tensor_input(input_details[0]['index'], [2, 224, 224, 3])
        interpreter.allocate_tensors()
        #image_resized = im.resize((width, height))
        #image_resized = np.expand_dims(image_resized, axis = 0).astype(np.float32)
        input_data = np.array(np.random.random_sample(input_shape), dtype=np.uint8)
        print("INPUT SHAPE: ", input_data.shape)
        interpreter.set_tensor(input_details[0]['index'], input_data)
        start = time.perf_counter()
        interpreter.invoke()
        inference_time = time.perf_counter() - start
        print('Inference %.2f ms' % (inference_time * 1000))
    # with timer.timeit():
    #     interpreter.invoke()

    output_details = interpreter.get_output_details()[0]
    output = np.squeeze(interpreter.get_tensor(output_details['index']))

    #k = int(args['kresults'])
    #top_k = output.argsort()[-k:][::-1]
    # result = []
    # for i in top_k:
    #     score = float(output[i] / 255.0)
    #     result.append((i, score))
    # image = put_info_on_frame(image, result, labels,
    #                           timer.time, args['model'], args['image'])

    #image = put_fps_on_frame(image,timer.timeit())

    #cv2.imshow(TITLE, image)
    #cv2.waitKey()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
          '--model',
          default='model/mobilenet_v1_1.0_224_quant.tflite',
          help='.tflite model to be executed')
    parser.add_argument(
          '--label',
          default='model/labels_mobilenet_quant_v1_224.txt',
          help='name of file containing labels')
    parser.add_argument(
          '--image',
          default='media/image.jpg',
          help='image file to be classified')
    parser.add_argument(
          '--kresults',
          default='3',
          help='number of displayed results')
    args = vars(parser.parse_args())
    image_classification(args)
